# Big Data

### 介绍

这是大数据的期末项目，我们小组做的是《吐槽吧》APP平台，主要为城市高压人群提供一个可以随心吐槽的平台，采用线上线下结合的方式，在线为用户
提供吐槽空间，匹配契合度高的聊天好友，以及为他们提供心理医生在线咨询服务，线下为用户挑选推荐合适的放松场所，以便他们放松身心。

### 参与贡献

1. 周三12-14 B2小组成员成为本仓库项目合作开发者
2. 小组成员：陈菲儿，吴慧晶，陈婷，许钰然，刘子琦，黄梓珊，孔晓桐
3. 各小组成员分工合作，各自新建markdown文件，提交他们的想法文章
4. 最后生成项目整合仓库Pages链接


### 项目内容文章

>    组员：刘子琦

- [伦理与参与：项目挑战与解决方案](https://gitee.com/windy-feier/Big-Data/blob/pr/challenge&solution.md)

>    组员：许钰然

- [想把解决的城市问题变成数据问题](https://gitee.com/windy-feier/Big-Data/blob/pr/problem.md)

>    组员：陈菲儿

- [产品原型的选择](https://gitee.com/windy-feier/Big-Data/blob/pr/%E4%BA%A7%E5%93%81%E5%8E%9F%E5%9E%8B%E7%9A%84%E9%80%89%E6%8B%A9.md)   

>    组员：孔晓桐，陈婷

- [数据伦理实验室](https://gitee.com/windy-feier/Big-Data/blob/pr/%E6%95%B0%E6%8D%AE%E4%BC%A6%E7%90%86%E5%AE%9E%E9%AA%8C%E5%AE%A4.md)

>    组员：陈菲儿

- [项目策划方案](https://gitee.com/windy-feier/Big-Data/blob/pr/%E9%A1%B9%E7%9B%AE%E7%AD%96%E5%88%92%E6%96%B9%E6%A1%88.md)

>    组员：吴慧晶

- [把城市发展问题变成数据问题](https://gitee.com/windy-feier/Big-Data/blob/pr/%E6%8A%8A%E5%9F%8E%E5%B8%82%E5%8F%91%E5%B1%95%E9%97%AE%E9%A2%98%E5%8F%98%E6%88%90%E6%95%B0%E6%8D%AE%E9%97%AE%E9%A2%98.md)
